# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name = 'fizzbuzz',
    version = '1.0.0',
    url = 'https://gitlab.com/alsagone/gl_tp.git',
    author = 'Hakim ABDOUROIHAMANE',
    author_email = 'hakim.abdouroihamane@etu.unistra.fr',
    description = 'Réalisation du TP 02',
    packages = find_packages(),
)