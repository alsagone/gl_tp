import random
import math


class FizzBuzz:

    def convert(self, number):
        return '{0}'.format(number)

    def fizzbuzz_output(self, number):
        fizz = (number % 3 == 0)
        buzz = (number % 5 == 0)

        output = ''

        if fizz:
            output += 'Fizz'

        if buzz:
            output += 'Buzz'

        if len(output) == 0:
            output += '{0}'.format(number)

        return output
