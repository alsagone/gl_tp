# -*- coding: utf-8 -*-
from unittest import TestCase
from fizzbuzz import FizzBuzz
import random


class FizzBuzzTest(TestCase):

    def setUp(self):
        self.fizzbuzz = FizzBuzz()

    def tearDown(self):
        self.fizzbuzz = None

    def test_returns_number_for_input_not_divisible_by_3_or_5(self):
        self.fizzbuzz = FizzBuzz()
        numbers_list = []

        while (len(numbers_list) < 7):
            random_number = random.randint(1, 1000)

            if (random_number % 3 != 0) and (random_number % 5 != 0):
                numbers_list.append(random_number)

        for n in numbers_list:
            self.assertEqual('{0}'.format(
                n),  self.fizzbuzz.fizzbuzz_output(n))

    def test_returns_fizz_for_input_divisible_by_3_not_by_5(self):
        self.fizzbuzz = FizzBuzz()
        expected = 'Fizz'

        numbers_list = []

        while (len(numbers_list) < 7):
            random_number = random.randint(1, 1000) * 3

            if (random_number % 5 != 0):
                numbers_list.append(random_number)

        for n in numbers_list:
            self.assertEqual(expected, self.fizzbuzz.fizzbuzz_output(n))

    def test_returns_fizz_for_input_divisible_by_5_not_by_3(self):
        self.fizzbuzz = FizzBuzz()
        expected = 'Buzz'

        numbers_list = []

        while (len(numbers_list) < 7):
            random_number = random.randint(1, 1000) * 5

            if (random_number % 3 != 0):
                numbers_list.append(random_number)

        for n in numbers_list:
            self.assertEqual(expected, self.fizzbuzz.fizzbuzz_output(n))

    def test_returns_fizz_for_input_divisible_by_3_and_by_5(self):
        self.fizzbuzz = FizzBuzz()
        expected = 'FizzBuzz'
        numbers_list = []

        while (len(numbers_list) < 7):
            random_number = random.randint(1, 1000) * 15
            numbers_list.append(random_number)

        for n in numbers_list:
            self.assertEqual(expected, self.fizzbuzz.fizzbuzz_output(n))
