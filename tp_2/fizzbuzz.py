def fizzbuzz_output(number):
        fizz = (number % 3 == 0)
        buzz = (number % 5 == 0)
    
        output = ''

        if fizz:
            output += 'Fizz'

        if buzz:
            output += 'Buzz'

        if len(output) == 0:
            output += '{0}'.format(number)

        return output 

def fizzbuzz():
    for n in range(1, 101, 1):
        output = fizzbuzz_output(n)

        if (n < 100):
            end_line = ', '

        else:
            end_line = '\n'

        print(output, end=end_line)

    return 

fizzbuzz()


        